angular.module('starter')

    .factory('User', function ($resource, backend) {
        return $resource(backend + '/user/signup',null,
            {
                'sendOtp': {//This api does not fetch video uploaded by loggedIn Player
                    method: 'GET',
                    url: backend + '/user/sendOtp/:phoneNumber',
                    isArray: false
                },
                'fbLogin': {//This api does not fetch video uploaded by loggedIn Player
                    method: 'POST',
                    url: backend + '/user/fbLogin',
                    isArray: false
                },
                'login': {//This api does not fetch video uploaded by loggedIn Player
                    method: 'POST',
                    url: backend + '/user/login',
                    isArray: false
                }
            }); // Note the full endpoint address
    })