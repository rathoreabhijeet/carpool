angular.module('starter')
    .service('facebookLogin',function(UserService,$q,loggedInUser,$ionicLoading,$state,User)
    {

// This method is to get the user profile info from the facebook api
        var getFacebookProfileInfo = function (authResponse) {
            var info = $q.defer();

            facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
                function (response) {

                    info.resolve(response);
                },
                function (response) {
                    info.reject(response);
                }
            );
            return info.promise;
        };

        // This is the success callback from the login method
        var fbLoginSuccess = function (response) {
            if (!response.authResponse) {
                fbLoginError("Cannot find the authResponse");
                return;
            }

            var authResponse = response.authResponse;

            getFacebookProfileInfo(authResponse)
                .then(function (profileInfo) {

                    console.log('after login')
                    console.dir(profileInfo)


// For the purpose of this example I will store user data on local storage
                    UserService.setUser({
                        authResponse: authResponse.authResponse,
                        userID: profileInfo.id,
                        name: profileInfo.name,
                        email: profileInfo.email
                    });


                    User.fbLogin({
                        'email': profileInfo.email,
                        'socialId': profileInfo.id,
                        'name': profileInfo.name
                    },function(data)
                    {
                        console.dir(data);
                        loggedInUser.info=data.user;

                    },function(err)
                    {
                        console.log(err);
                    });

                }, function (err) {
                    $ionicLoading.hide();

                    console.log(5)
                });
        };

// This is the fail callback from the login method
        var fbLoginError = function (error) {
            $ionicLoading.hide();
            console.log(error)
            // $cordovaToast.showShortBottom('Error connecting to Facebook')
        };

//This method is executed when the user press the "Login with facebook" button
        this.facebookSignIn = function () {

            facebookConnectPlugin.getLoginStatus(function (success) {
                if (success.status === 'connected') {

// The user is logged in and has authenticated your app, and response.authResponse supplies
// the user's ID, a valid access token, a signed request, and the time the access token
// and signed request each expire

// Check if we have our user saved
                    var user;
                    user = UserService.getUser('facebook');

                    if (!user.userID) {
                        getFacebookProfileInfo(success.authResponse)
                            .then(function (profileInfo) {

// For the purpose of this example I will store user data on local storage
                                UserService.setUser({
                                    authResponse: success.authResponse,
                                    userID: profileInfo.id,
                                    name: profileInfo.name,
                                    email: profileInfo.email
                                });


                                //call an api to get info or this user. send social Id

                                // loggedInUser.info = {
                                //     'email': user.email,
                                //     'userId': user.userID,
                                //     'name': user.name
                                // };
                                user = UserService.getUser('facebook');

                                console.log('if')
                                console.dir(user)

                                $state.go('categories')

                            }, function (fail) {
// Fail get profile info
                            });
                    }
                    else {


                        //call an api to get info or this user. send social Id

                        // loggedInUser.info = {
                        //     'email': user.email,
                        //     'userId': user.userID,
                        //     'name': user.name
                        // };
                        console.log('else')

                        $state.go('categories')


                    }
                }
                else {
// If (success.status === 'not_authorized') the user is logged in to Facebook,
// but has not authenticated your app
// Else the person is not logged into Facebook,
// so we're not sure if they are logged into this app or not.


                    $ionicLoading.show({
                        template: 'Logging in...'
                    });

// Ask the permissions you need. You can learn more about
// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
                    facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
                }
            });
        };
    })
    .service('UserService', function () {
        // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
        var setUser = function (user_data) {
            window.localStorage.starter_facebook_user = JSON.stringify(user_data);
        };

        var getUser = function () {
            return JSON.parse(window.localStorage.starter_facebook_user || '{}');
        };

        return {
            getUser: getUser,
            setUser: setUser
        };
    })
