angular.module('starter')
    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom')
        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.transition('ios');
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in bookingCtrl.js
        $stateProvider

        // setup an abstract state for the tabs directive
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'loginCtrl',
                controllerAs: 'login'
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'templates/signup.html',
                controller: 'signupCtrl',
                controllerAs: 'signup'
            })
            .state('menu', {
                url: '/menu',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'menuCtrl',
                controllerAs: 'menu'
            })
            .state('menu.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'homeCtrl',
                        controllerAs: 'home'
                    }
                }
            })
          .state('menu.post', {
            url: '/post',
            views: {
              'menuContent': {
                templateUrl: 'templates/post.html',
                controller: 'postCtrl',
                controllerAs: 'post'
              }
            }
          })
            .state('menu.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/profile.html',
                        controller: 'profileCtrl',
                        controllerAs: 'profile'
                    }
                }
            })
            .state('askPhoneNumber', {
                url: '/askPhoneNumber',
                templateUrl: 'templates/askPhoneNumber.html',
                controller: 'askPhoneNumberCtrl',
                controllerAs: 'askPhoneNumber'
            })
            .state('emailLogin', {
                url: '/emailLogin',
                templateUrl: 'templates/emailLogin.html',
                controller: 'emailLoginCtrl',
                controllerAs: 'login'
            })
            .state('otp', {
                url: '/otp',
                templateUrl: 'templates/otp.html',
                controller: 'otpCtrl',
                controllerAs: 'otp'
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');


        //$urlRouterProvider.otherwise('/player/sidemenu/choosesport');

    })
