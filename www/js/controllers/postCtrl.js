angular.module('starter')

.controller('postCtrl', function($timeout,$ionicModal,$scope) {

  var post = this;
  var devHeight = window.innerHeight;
  var devWidth = window.innerWidth;
  post.poolOptionsVisible = false;
  post.showPoolNeedButton = false;
  post.showPoolGiveButton = false;
  post.halfDim = {'height':0.4*devHeight+'px','width': devWidth+'px'};
  post.detailDiv = {'min-height':0.6*devHeight-44+'px','width': devWidth+'px'};
  post.fullDimWithHeader = {'height':devHeight-88+'px','width': devWidth+'px'};

  post.selectedDays = [];

  post.chooseOrigin = function(){
    post.originSaved = true;
  }
  post.chooseDestination = function(){
    post.destinationSaved = true;
  }
  post.chooseType = function(type){
    post.type = type;
  }
  post.selectDay = function(day){
    if(_.indexOf(post.selectedDays,day)==-1) {
      post.selectedDays.push(day);
    }
    else{
      post.selectedDays = _.reject(post.selectedDays,function(n){
        return n==day;
      });
    }
  }
  post.dayIsSelected = function(day){
    if(_.indexOf(post.selectedDays,day)!=-1){
      return true;
    }
    else{
      return false;
    }
  }
})
