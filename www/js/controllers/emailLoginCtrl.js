angular.module('starter')

    .controller('emailLoginCtrl', emailLoginCtrl)
function emailLoginCtrl($window, $state,User) {
    var login = this;
    console.log($state.current.name)
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    login.logoDiv = {
        'margin-top': 0.04 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.03 * devHeight + 'px'
    };
    login.formDiv = {'margin-top': 0.01 * devHeight + 'px'};
    login.fullHeight = {'height': devHeight + 'px'};
    login.forgotDiv = {'width': 100 + '%'};
    login.forgotMargin = {'margin-top': 0.01 * devHeight + 'px'};
    login.signup = {'margin-top': 0.01 * devHeight + 'px'};
    login.username = {'margin-bottom': 0.01 * devHeight + 'px'};
    login.socialLoginButton = {'height': 0.1 * devHeight + 'px'};


    //-------------------------DESIGN CALCULATION END-------------------------//

    login.loginFunction=function()
    {
        User.login(login.user,function(result)
        {
            console.log(result);
        },function(err)
        {
            console.log(err);
        })

    }

}
