angular.module('starter')

.controller('loginCtrl', function($state,facebookLogin) {

  var login = this;
  var devHeight = window.innerHeight;
  var devWidth = window.innerWidth;

  login.fulldim = {'height':devHeight+'px','width': 0.8*devWidth+'px'};

  login.facebookLogin=function()
  {
    facebookLogin.facebookSignIn();
    $state.go('menu.home');
  }



  login.googleLogin=function()
  {
    $state.go('menu.home');
  }

  login.goToSignUp=function()
  {
    $state.go('signup');
  }

  login.goToLogin=function()
  {
    $state.go('emailLogin');
  }



})
