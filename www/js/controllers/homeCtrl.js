angular.module('starter')

.controller('homeCtrl', function($timeout,$ionicModal,$scope) {

  var home = this;
  var devHeight = window.innerHeight;
  var devWidth = window.innerWidth;
  home.poolOptionsVisible = false;
  home.showPoolNeedButton = false;
  home.showPoolGiveButton = false;
  home.halfDim = {'height':0.4*devHeight+'px','width': devWidth+'px'};
  home.detailDiv = {'min-height':0.6*devHeight-44+'px','width': devWidth+'px'};
  home.fullDimWithHeader = {'height':devHeight-88+'px','width': devWidth+'px'};

  home.showPoolOptions = function(){
    home.rentalOptionsVisible = false;
    home.showRentalNeedButton = false;
    home.showRentalGiveButton = false;

    home.poolOptionsVisible = true;
    home.showPoolNeedButton = true;
    $timeout(function(){
      home.showPoolGiveButton = true;
    },200)
  }
  home.showRentalOptions = function(){
    home.poolOptionsVisible = false;
    home.showPoolNeedButton = false;
    home.showPoolGiveButton = false;

    home.rentalOptionsVisible = true;
    home.showRentalGiveButton = true;
    $timeout(function(){
      home.showRentalNeedButton = true;
    },200)
  }
  home.openPostOptionModal = function(){
    $ionicModal.fromTemplateUrl('templates/postOptionModal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function (modal) {
        home.postOptionModal = modal;
        home.postOptionModal.show();
      });
  }
  home.closePostOptionModal = function(){
    home.postOptionModal.hide();
    home.postOptionModal.remove();
  }
})
