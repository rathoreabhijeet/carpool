angular.module('starter')

    .controller('signupCtrl',signupCtrl)
function signupCtrl( $window, $state,$ionicLoading,$timeout,User,
                    loggedInUser) {
    var signup = this;
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    signup.signup = {'height': 0.08 * devHeight + 'px','margin-top':0.01*devHeight+'px'};
    signup.logoDiv = {'margin-top': 0.1 * devHeight + 'px', 'height': 0.2 * devHeight + 'px','margin-bottom':0.04*devHeight+'px'};
    signup.formDiv = {'margin-top': 0.02 * devHeight + 'px'};
    signup.fullHeight = {'height': devHeight + 'px'};
    signup.paddingHorizontal = {'padding-left':0.1*devWidth+'px','padding-right':0.1*devWidth+'px'}
    signup.joinButton = {'width': 100 + '%', 'margin-bottom': 0.02 * devHeight + 'px',
        'padding-left':0.1*devWidth+'px','padding-right':0.1*devWidth+'px'};
    signup.socialLoginButton = {'height': 0.1 * devHeight + 'px'};

    //-------------------------DESIGN CALCULATION END-------------------------//

    signup.showForm=true;

    var OTPFromBackend;
    signup.verifyPhoneNumber=function()
    {

            User.sendOtp({phoneNumber:signup.phoneNumber},
                function(result)
            {
                OTPFromBackend=result.otp;
                console.log(OTPFromBackend);
                signup.showForm=false;
            },function(err)
            {
                console.log(err);
            })
        
    }
    signup.signUpFunction=function() {
        signup.invalidUser=false;
        signup.invalidOTP=false;
        if(OTPFromBackend==signup.user.OTP)
        {
            User.save(signup.user,function(result)
            {
                console.log(result);
            },function(err)
            {
                console.log(err);
                if(err.data='Unauthorized')
                {
                    signup.invalidUser=true;

                    $timeout(function()
                    {
                        signup.invalidUser=false;
                    },3000);
                }
            })
        }
        else {
            signup.invalidOTP=true;
            $timeout(function()
            {
                signup.invalidOTP=false;
            },3000);
            console.log('invalid otp');
        }
    }


}
