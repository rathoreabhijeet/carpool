angular.module('starter')

.controller('profileCtrl', function($state) {

  var profile = this;
  var devHeight = window.innerHeight;
  var devWidth = window.innerWidth;

  profile.pictureDiv = {'height':0.4*devHeight+'px','width': devWidth+'px'};
  profile.detailDiv = {'min-height':0.6*devHeight-44+'px','width': devWidth+'px'};

})
